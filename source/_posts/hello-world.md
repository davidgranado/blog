---
title: Hello World...Again
date: 2016-10-02 22:40:01
tags: [ ramblings ]
---
It's been a while since I've had a place to put my thoughts.  My previous site met an unfortunate end while running an upgrade on the server OS.  Coincidentally, that's also when I discovered that my backups...well...didn't exist.  Oops.  Shoulda double checked on that.

### You didn't miss much
Since then, I've been too lazy to set another one up.  But, as of late, I've been itching to clarify my thoughts by airing them.  If I'd gotten around to this a year ago, it would have been all things React.  Likely stuff along the lines of "a kitten dies every time you mix Flux or Redux logic with presentation components" or "Hitler used stateful components".  You know...the standard battles in the modern javascript world that function mostly for self-gratification.

<!-- more -->

While I actually _do_ enjoy architecture talks, we need to be aware of the cost associated with them. Software engineers could spend days debating the "correct" way to do things. But during these debates, we sometimes need to be reminded that these are to serve the user's needs, whether directly or indirectly.  The point of well architected code is to make it easier for you and the team to maintain the code.  But there comes a point when the time spent arguing about architecture begins to exceed the likely cost of maintaining code that gets the job done **now**.

### It's a different world

In recent times, I've picked up game development again.  It's quite refreshing to read articles and forum discussions that more closely revolve around the end-user experience rather than religious statements.

When it comes to games, you have no real sense of how fun your idea is until you implement it (or...in the case of my work, how much cancer it gives you).  You could spend forever building the most pristinely coded game ever, just to realize that all your effort accomplished nothing more than making Superman 64 look fun.

To prevent this, one builds a prototype (not to be confused with minimum viable products) that proves out the core idea.  It's a great exercise that helps dust off the practical side of you brain that got lost somewhere in the ivory tower.

### An exercise in practicality

That said, at this point in time, I have the game design skills of an 8 month old.  That's due to my lack of any artistic creativity.  But _prototyping_ is a more concrete skill that can be developed by all software engineers.  How often have you been part of a monolithic project that was released without the ROI that was expected?

### My own personal landfill

To better develop my prototyping senses, I'm going to start dumping (the most fitting word) my prototypes here.  I'll start by finding as many of my old ones as I can and post them along with a blurb about where my head was.  Then, I'll do the same for any moving forward.  If I continue to iterate, I'll post the iterations as either branches or tags to the repo.
