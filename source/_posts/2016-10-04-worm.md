---
title: '"Worm" (2009): My "How are you?" of Games'
date: 2016-10-04 21:51:17
tags: [ gamedev, programming, javascript, retrospective ]
---

[Play _Worm_ (2009)](/projects/2016-10-03/Worm/)

After I made {% post_link snake '"Snake"'%}, I took it to work the next day to show my coworkers.  They all seemed interested, asking questions and looking at the code.  Then, one issued an interesting challenge: "Dude, you should make 'worm'".  The idea was an interesting incremental change.  Rather than occupying discrete blocks within a grid like the snake, the worm position values would be continuous.

I was intrigued and pumped out the change that night.

<!-- more -->

### Implementation notes

Working with a grid made things really easy for "snake".  But it was a little more complicated for "worm".

I decided to try and map over some of the high-level ideas.  I rendered the snake as a series of circles rather than boxes.  That worked, but it looked more like a bug of some sort rather than a worm.  But if I rendered the circles more closely together, it would look more like worm-y.  If the head circle overlaps any of the "body" circles, then the worm has eaten itself.  However, this check starts a number of circles down the list since the circles are so tightly packed.

For the movement, rather than picking a direction and filling in the next box, I had to transition between directions.  That is, if you were heading east, and you press "up", rather than instantly turn north, it smoothly slithers toward the target direction.  It stops once you either reach the direction or you release the button.

The source is available on [GitLab](https://gitlab.com/davidgranado/Worm/ "David Granado / Worm - GitLab" target="_blank").
<a href="https://gitlab.com/davidgranado/Worm/" target="_blank">
	<img src="/images/wm_no_bg.png" alt="Fork it on GitLab">
</a>
